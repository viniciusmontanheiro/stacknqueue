#include<string>


#include <iostream>

using namespace std;

#include"queue.h";
#include"stack.h";
#include"stackvector.h";
#include"queuevector.h";

int main()
{

    int operacao;
    int tamanho;

    cout<<"[ESCOLHA A ESTRUTURA QUE DESEJA CRIAR]"<<endl;
    cout<<" 0 - PILHA COM VETOR "<<endl;
    cout<<" 1 - FILA COM VETOR "<<endl;
    cout<<" 2 - PILHA COM ENCADEAMENTO "<<endl;
    cout<<" 3 - FILA COM ENCADEAMENTO "<<endl;
    cout<<" 4 - SAIR"<<endl;
    cin>>operacao;


    if(operacao == 0){
        StackVector stackV;
        cout<<"\nInforme o tamanho do vetor:";
        cin>>tamanho;
        stackV.init(tamanho);

        while(true){
            cout<<"\n[AÇOES]\n"<<endl;
            cout<<" 0 - INSERIR\n"<<endl;
            cout<<" 1 - RETIRAR\n"<<endl;
            cout<<" 2 - VERIFICAR PILHA VAZIA\n"<<endl;
            cout<<" 3 - IMPRIMIR\n"<<endl;
            cout<<" 4 - SAIR\n"<<endl;
            cin>>operacao;


             switch (operacao){
                 case 0:
                      int item;

                      cout<<"\nInforme o numero:";
                      cin>>item;
                      stackV.push(item);
                      break;
                 case 1:
                      cout<<"\nO valor " << stackV.pop() << " foi retirado!"<<endl;
                      break;
                 case 2:
                      stackV.isEmpty() ? cout<<"Pilha vazia!" : cout<<"Pilha nao vazia!";
                      break;
                 case 3:
                      cout<<"[PILHA]"<<endl;
                      stackV.show();
                      break;
                 case 4:
                      exit(1);
                      break;
            }
        }

    }else{
        if(operacao == 1){
            QueueVector queueV;
            cout<<"\nInforme o tamanho do vetor:";
            cin>>tamanho;
            queueV.init(tamanho);


            while(true){
                cout<<"\n[AÇOES]\n"<<endl;
                cout<<" 0 - INSERIR\n"<<endl;
                cout<<" 1 - RETIRAR\n"<<endl;
                cout<<" 2 - VERIFICAR PILHA VAZIA\n"<<endl;
                cout<<" 3 - IMPRIMIR\n"<<endl;
                cout<<" 4 - SAIR\n"<<endl;
                cin>>operacao;


                 switch (operacao){
                     case 0:
                          int item;
                          cout<<"\nInforme o numero:";
                          cin>>item;
                          queueV.enqueue(item);
                          break;
                     case 1:
                          cout<<"\nO valor " << queueV.dequeue() << " foi retirado!"<<endl;
                          break;
                     case 2:
                           queueV.isEmpty() ? cout<<"Pilha vazia!" : cout<<"Pilha nao vazia!";
                          break;
                     case 3:
                          cout<<"[FILA]"<<endl;
                          queueV.show();
                          break;
                     case 4:
                          exit(1);
                          break;
                }
            }
        }else{
            if(operacao == 2){
                Stack stack;
                stack.init();

                while(true){
                    cout<<"\n[AÇOES]\n"<<endl;
                    cout<<" 0 - INSERIR\n"<<endl;
                    cout<<" 1 - RETIRAR\n"<<endl;
                    cout<<" 2 - VERIFICAR PILHA VAZIA\n"<<endl;
                    cout<<" 3 - IMPRIMIR\n"<<endl;
                    cout<<" 4 - SAIR\n"<<endl;
                    cin>>operacao;


                     switch (operacao){
                         case 0:
                              int item;
                              cout<<"\nInforme o numero:";
                              cin>>item;
                              stack.push(item);
                              break;
                         case 1:
                              cout<<"\nO valor " << stack.pop() << " foi removido!"<<endl;
                              break;
                         case 2:
                              stack.isEmpty() ? cout<<"Pilha vazia!" : cout<<"Pilha nao vazia!";
                              break;
                         case 3:
                              cout<<"[STACK]"<<endl;
                              stack.show();
                              break;
                         case 4:
                              exit(1);
                              break;
                    }
                }
            }else{
                if(operacao == 3){
                    Queue queue;
                    queue.init();

                    while(true){
                        cout<<"\n[AÇOES]\n"<<endl;
                        cout<<" 0 - INSERIR\n"<<endl;
                        cout<<" 1 - RETIRAR\n"<<endl;
                        cout<<" 2 - VERIFICAR PILHA VAZIA\n"<<endl;
                        cout<<" 3 - IMPRIMIR\n"<<endl;
                        cout<<" 4 - SAIR\n"<<endl;
                        cin>>operacao;


                         switch (operacao){
                             case 0:
                                  int item;
                                  cout<<"\nInforme o numero:";
                                  cin>>item;
                                  queue.enqueue(item);
                                  break;
                             case 1:
                                  cout<<"\nO valor " << queue.dequeue() << " foi removido!"<<endl;
                                  break;
                             case 2:
                                   queue.isEmpty() ? cout<<"Pilha vazia!" : cout<<"Pilha nao vazia!";
                                  break;
                             case 3:
                                  cout<<"[FILA]"<<endl;
                                  queue.show();
                                  break;
                             case 4:
                                  exit(1);
                                  break;
                        }
                    }

                }else{
                    exit(1);
                }
            }
        }
    }

    return 0;
}

