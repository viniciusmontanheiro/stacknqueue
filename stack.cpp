#include"stack.h"

using namespace std;

Stack::Stack() {}

/*
*@description Método ini: inicia os valores
*/
void Stack::init(){
     this->top = NULL;
}

/*
 *@description Método push: insere um item na pilha
 *@return boolean
 */
void Stack::push(int item) {
    Node *node = new Node;

    if(!node){
        cerr<<"\n[Nao foi possivel inserir o item na pilha!]\n";
        exit(1);
    }
    node-> item = item;
    node->next = this->top;
    this->top = node;
}

/*
 *@description Método pop: remove o item da pilha
 *@return boolean
 */
int Stack::pop() {
    Node *node;
    int item;

    if(!this->top){
        cerr<<"\n[A pilha está vazia!!]\n";
        exit(1);
    }

    node = this->top;
    this->top = node->next;
    item = node->item;
    delete node;
    return item;
}

/*
 *@description Método show : exibe a pilha por nó
 *@return boolean
 */
void Stack::show(){

    Node *node = new Node;
    node = this->top;

    while(node){
         cout<<node->item<<endl;
          node = node->next;
     }
     cout<<endl;
}

/*
 *@description Método isEmpty : verifica se a pilha está vazia
 *@return boolean
 */
bool Stack::isEmpty(){

    Node *node = new Node;
    node = this->top;
    return node->item ? false : true;
}



