#include "stackvector.h"

StackVector::StackVector(){
    this->position = 0;
}

/*
*@description Método init : Define o tamanho da pilha e aloca o vetor
*@params int
*/
void StackVector::init (int size)
{
    this->size = size;
    this->vector = new int[this->size];
}

/*
 *@description Método push: Recebe uma pilha e adiciona o item na mesma
 *@params StackVector*, int
 */
void StackVector::push (int item){

     if (this->position == this->size) { //Verifica se a pilha está cheia
            cerr<<"\n[A pilha está cheia!!]\n";
            exit(1);
     }

     this->vector[this->position] = item; //Adicionando o item na pilha
     this->position++; //Apontamos para próxima posição
}

/*
 *@description Método pop: retorna o item do topo da pilha.
 *@return int
 */
int StackVector::pop (){
    int item;

     if (this->isEmpty()) {  //Verificamos se a pilha está vazia
        cerr<<"\nA pilha está vazia!\n";
        exit(1);
     }

     this->position--; //Delegando o penultimo como elemento do topo
     item = this->vector[this->position];

    return item;
}

/*
 *@description Método isEmpty: verifica se a pilha está vazia
 *@return boolean
 */
bool StackVector::isEmpty (){
    if(this->position >= 1 ){
        return false;
    }
    return true;
}

/*
 *@description Método show: adicionamos os itens em uma pilha auxiliar,
 * depois exibimos esses itens
 *@return boolean
 */
void StackVector::show(){
    int item;
    StackVector* aux = new StackVector;
    aux->init(this->size);
    while(this->position >= 1){
        item = this->pop();
         aux->push(item);
         cout << item<<endl;
    }
    cout<<endl;
    this->size = aux->size;
    this->position = aux->position;
    this->vector = aux->vector;
}

