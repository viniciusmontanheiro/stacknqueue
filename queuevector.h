#ifndef QUEUEVECTOR_H
#define QUEUEVECTOR_H

#include <iostream>
#include <stdlib.h>

using namespace std;

class QueueVector
{
private:
    int position;
    int size;
    int* vector;

public:
    QueueVector();
    void init(int size);
    void enqueue(int item);
    int dequeue();
    bool isEmpty();
    void show();
};


#endif // QUEUEVECTOR_H
