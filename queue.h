#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>
#include <stdlib.h>

using namespace std;

#include"node.h"

class Queue
{
public:

    Node *head;
    Node *tail;

    Queue();
    void init();
    void enqueue(int item);
    int dequeue();
    void show();
    bool isEmpty();
};

#endif // QUEUE_H
