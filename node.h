#ifndef NODE_H
#define NODE_H

#include <iostream>

using namespace std;

class Node {
public:
    int item;
    Node *next;
    Node();
};

#endif // NODE_H
