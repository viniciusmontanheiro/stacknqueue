#include "queue.h"

Queue::Queue(){}

/*
*@description Método init, inicia os valores
*@return boolean
*/
void Queue::init(){
    this->head = NULL;
    this->tail = NULL;
}

/*
 *@description Método enqueue, insere um item no inicio da fila
 *@return boolean
 */
void Queue::enqueue(int item)
{
    Node *node = new Node;
    if(!node){
        cerr<<"\n[Nao foi possivel inserir o item na fila!]\n";
        exit(1);
    }
    if(!tail){
        head = node;
    }
    else{
        tail->next = node;
    }
    node->item = item;
    tail = node;
}

/*
 *@description Método dequeue, remove o primeiro da fila;
 *@return boolean
 */
int Queue::dequeue()
{
    int item;
    if(!this->head){
       cerr<<"\n[A fila está vazia!]\n";
       exit(1);
    }

    Node *node = this->head;
    this->head = this->head->next;

    if(!this->head){
       tail = this->head;
    }
    item = node->item;
    delete node;
    return item;
};

/*
 *@description Método show : exibe a pilha por nó
 *@return boolean
 */
void Queue::show(){

    Node *node = new Node;
    node = this->head;

    while(node){
         cout<<node->item<<" ";
          node = node->next;
     }
     cout<<endl;

}

/*
 *@description Método isEmpty : verifica se a fila está vazia
 *@return boolean
 */
bool Queue::isEmpty(){

    Node *node = new Node;
    node = this->head;
    return node->item ? false : true;
}
