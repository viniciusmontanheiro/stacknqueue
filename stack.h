#ifndef STACK_H
#define STACK_H

#include <iostream>
#include <stdlib.h>

using namespace std;
#include"node.h"

class Stack
{
public:
    Stack();
    Node *top;
    void init();
    void push(int item);
    int pop();
    void show();
    bool isEmpty();
};

#endif // STACK_H
