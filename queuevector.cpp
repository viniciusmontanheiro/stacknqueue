#include "queuevector.h"

QueueVector::QueueVector(){
    this->position = 0;
}

/*
*@description Método create : Define o tamanho da fila e aloca o vetor
*@params int
*/
void QueueVector::init (int size)
{
    this->size = size;
    this->vector = new int[this->size];
}

/*
 *@description Método queue: Insere um item na fila
 *@params QueueVector*, int
 */
void QueueVector::enqueue (int item){

     if (this->position == this->size) { //Verifica se a pilha está cheia
            cerr<<"\n[A fila está cheia!!]\n";
            exit(1);
     }

     this->vector[this->position] = item; //Adicionando o item na fila
     this->position++; //Apontamos para próxima posição
}

/*
 *@description Método dequeue : Remove o primeiro da fila
 *@return int
 */
int QueueVector::dequeue (){
    int item;

     if (this->isEmpty()) {  //Verificamos se a fila está vazia
        cerr<<"\n[A fila está vazia!]\n";
        exit(1);
     }

     this->position--;
     item = this->vector[0]; //Pegando o primeiro elemento

     for(int i = 0;i <= this->position; i++){
         this->vector[i] = this->vector[i + 1];
     }

    return item;
}

/*
 *@description Método isEmpty: verifica se a pilha está vazia
 *@return boolean
 */
bool QueueVector::isEmpty (){
    if(this->position >= 1 ){
        return false;
    }
    return true;
}

/*
 *@description Método show: adicionamos os itens em uma fila auxiliar,
 * depois exibimos o itens
 *@return boolean
 */
void QueueVector::show(){
    int item;
    QueueVector* aux = new QueueVector;
    aux->init(this->size);
    while(this->position >= 1){
        item = this->dequeue();
         aux->enqueue(item);
         cout << item<<" ";
    }
    cout<<endl;
    this->size = aux->size;
    this->position = aux->position;
    this->vector = aux->vector;
}

