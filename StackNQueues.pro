TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    stackvector.cpp \
    queuevector.cpp \
    stack.cpp \
    node.cpp \
    queue.cpp

HEADERS += \
    stackvector.h \
    queuevector.h \
    stack.h \
    node.h \
    queue.h

