#ifndef STACKVECTOR_H
#define STACKVECTOR_H

#include <iostream>
#include <stdlib.h>

using namespace std;

class StackVector
{
private:
    int position;
    int size;
    int* vector;

public:
    StackVector();
    void init(int size);
    void push(int item);
    int pop();
    bool isEmpty();
    void show();
};

#endif // STACKVECTOR_H
